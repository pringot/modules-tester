#!/bin/bash

set -eu

. ./setup.sh

cc=$(jq -r '.|.gpu_devices|[.[]]|first|.compute_capability' /etc/grid5000/ref-api.json)

(cd src; export GPU_ARCHITECTURE_OPT="--gpu-architecture=sm_${cc/./}";  make -e)
