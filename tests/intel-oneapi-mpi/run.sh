#!/bin/bash

set -ueo pipefail

. ./setup.sh

echo -n "Path of mpirun: "
which mpirun
[ $(which mpirun) != '/usr/bin/mpirun' ]
mpirun --version
