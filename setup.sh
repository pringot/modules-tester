#!/bin/bash

# Common setup file

progname=$(basename $0)

function usage_setup {
  2>&1 echo "usage: ${progname} module version"
  exit 1
}

function get_module_name {
  if [ $2 == 'DEFAULT' ] ; then
    name=$1
  else
    name=$1/$2
  fi
  echo $name  
}

function load_module {
  local modname=$(get_module_name $1 $2)
  module load $modname
}

function load_gcc_of_module {
  local modname=$(get_module_name $1 $2)
  local gccused=$(module list $modname 2>&1 |grep '1)'|awk -F_ '{print $2}')
  local gccmod=$(module av ${gccused/-//} 2>&1 |grep ${gccused/-//})
  module load $gccmod
}
